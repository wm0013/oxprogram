
public class CheckWin extends OXMain{

    boolean stop;
    Turn turn;

    public CheckWin() {
        stop = false;
    }

    public boolean getStop() {
        return stop;
    }

    public void setStop(boolean input) {
        stop = input;
    }

    public boolean checkInput(String input) {
        if (input.equals("1") || input.equals("2") || input.equals("3")) {
            return false;
        } else {
            System.out.println("Please enter only numeric values between 1 and 3. ");
            return true;
        }
    }

    public void showDraw(int turn) {
        if ((turn == 9) && (stop == false)) {
            System.out.println("Draw!!!");
            stop = true;
        }
    }

    public void checkWin() {
        boolean stop1, stop2, stop3, stop4;
        stop1 = checkDiadonalLeft();
        stop2 = checkDiadonalRight();
        for (int num = 1; num < 4; num++) {
            stop3 = checkRow(num);
            stop4 = checkColumn(num);
            if (stop1 == true || stop2 == true || stop3 == true || stop4 == true) {
                stop = true;
            } else {
                stop = false;
            }
        }
    }

    public boolean checkDiadonalLeft() {
        return board.board[1][1].equals(board.board[2][2]) && board.board[1][1].equals(board.board[3][3]) && !board.board[1][1].equals("-");
    }

    public boolean checkDiadonalRight() {
        return board.board[1][3].equals(board.board[2][2]) && board.board[1][3].equals(board.board[3][1]) && !board.board[1][3].equals("-");
    }

    public boolean checkColumn(int col) {
        return board.board[1][col].equals(board.board[2][col]) && board.board[1][col].equals(board.board[3][col]) && !board.board[1][col].equals("-");
    }

    public boolean checkRow(int row) {
        return board.board[row][1].equals(board.board[row][2]) && board.board[row][1].equals(board.board[row][3]) && !board.board[row][1].equals("-");
    }

    public void showWin(String player) {
        if (stop == true) {
            System.out.println(player + " Win");
        }
    }
}
