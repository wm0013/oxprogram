
import java.util.Scanner;

public class OXMain {

    static boolean gameEnd = true;
    static Board board;
    static Player player;
    static int p1 = 0, p2 = 0, d = 0;
    static Scanner keyBoard = new Scanner(System.in);

    private static void showWelcome() {
        System.out.println("----------------------------------------");
        System.out.println("Welcome to OX Game.");
    }

    private static void setWinCount(int num) {
        if (num == 1) {
            p1++;
        } else if (num == 2) {
            p2++;
        } else if (num == 3) {
            d++;
        }

    }

    private static void showCount() {
        System.out.println("-------------------------------");
        System.out.println("   Player1 Win = " + p1);
        System.out.println("   Player2 Win = " + p2);
        System.out.println("   Draw = " + d);
        System.out.println("-------------------------------");

    }

    private static void showContinue() {
        System.out.print("Do you want to continue playing this game?(Yes/Y/No/N): ");
    }

    private static boolean playAgain(String answer) {
        boolean gameConinue;
        if (answer.equalsIgnoreCase("yes") || answer.equalsIgnoreCase("y")) {
            gameConinue = true;
        } else if (answer.equalsIgnoreCase("no") || answer.equalsIgnoreCase("n")) {
            gameConinue = false;
        } else {
            System.out.print("Please answer as follows: Yes, yes, No, no, y, Y, n, N: ");
            gameConinue = playAgain(keyBoard.next());
        }
        return gameConinue;
    }

    private static void thankYou() {
        System.out.println("Thank you for playing our game.");
    }

    public static void main(String[] args) {
        do {
            board = new Board();
            showWelcome();
            board.getBoard();
            String choose;
            do {
                choose = board.choosePlay();
                setWinCount(board.addCount());
            } while (!(choose.equalsIgnoreCase("o")) && !(choose.equalsIgnoreCase("x")));
            showCount();
            showContinue();
            gameEnd = playAgain(keyBoard.next());
        } while (gameEnd);
        thankYou();
    }
}
